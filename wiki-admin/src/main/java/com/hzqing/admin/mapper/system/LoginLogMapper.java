package com.hzqing.admin.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzqing.admin.model.entity.system.LoginLog;

/**
 * @author hzqing
 * @date 2019-05-17 09:55
 */
public interface LoginLogMapper extends BaseMapper<LoginLog> {

}
